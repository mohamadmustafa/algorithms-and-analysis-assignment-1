# README #

### What is this repository for? ###

* This repository was created to prove the principle "Six degrees of seperation" using different data structures and search algorithms.

### Operation details ###

* <operation> [arguments]
* where operation is one of {AV, AE, N, RV, RE, V, E, S, Q} and arguments is for optional arguments of some of the operations. The operations take the following form:
* AV <vertLabel> – add a vertex with label ’vertLabel’ into the graph.
* AE <srcLabel> <tarLabel> – add an edge with source vertex ’srcLabel’ and target vertex ’tarLabel’ into the graph.
* N <vertLabel> – Return a set of neighbours for vertex ’vertLabel’. The ordering of the neighbours does not matter. See below for the required format.
* RV <vertLabel> – remove vertex ’vertLabel’ from the graph.
* RE <srcLabel> <tarLabel> – remove edge with source vertex ’srcLabel’ and target vertex ’tarLabel’ from the graph.
* V – prints the vertex set of the graph. See below for the required format. The vertices can be printed in any order.
* E – prints the edge set of the graph. See below for the required format. The edges can be printed in any order.
* S <vertLabel1> <vertLabel2> – compute the shortest path distance between vertex ’vertLabel1’ and vertex ’vertLabel2’. If there is no path between the two vertices, then their distance is -1. See below for the required format.
* Q – quits the program


### Who do I talk to? ###

* Assignment owned by RMIT
* Contributers are Mohamad Mustafa and Scott Virtue.