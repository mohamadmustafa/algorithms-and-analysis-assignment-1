import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * Adjacency list implementation for the FriendshipGraph interface.
 * 
 * Your task is to complete the implementation of this class. You may add
 * methods, but ensure your modified class compiles and runs.
 *
 * @author Jeffrey Chan, 2016.
 */
public class AdjList<T extends Object> implements FriendshipGraph<T> {
	
	private int arrayCapacityCurrent = 10;
	
	private int verticesCount = 0;
	
	Map<String, Integer> vertexLabelToIndexMap;
	
	MyLinkedList[] myAdjList;

	/**
	 * Contructs empty graph.
	 */
	public AdjList() {
		myAdjList = new MyLinkedList[arrayCapacityCurrent];
		vertexLabelToIndexMap = new HashMap<>();
	} // end of AdjList()

	public void addVertex(T vertLabel) {
		if (!vertexExist(vertLabel)) {
			addNewVertexToAdjList(vertLabel);
		}
	} // end of addVertex()

	public void addEdge(T srcLabel, T tarLabel) {
		if(canEdgeBeAdded(srcLabel,tarLabel)){ 
			addNewEdgeToVertex(srcLabel,tarLabel);
		}
	} // end of addEdge()

	public ArrayList<T> neighbours(T vertLabel) {
		return getAllNeighbours(vertLabel);
	} // end of neighbours()

	public void removeVertex(T vertLabel) {
		removeVertexFromAdjList(vertLabel);
	} // end of removeVertex()

	public void removeEdge(T srcLabel, T tarLabel) {
		removeEdgeFromVertex(srcLabel,tarLabel);
	} // end of removeEdges()

	public void printVertices(PrintWriter os) {
		
		if (vertexLabelToIndexMap.isEmpty()) return;
		
		StringBuffer vertices = new StringBuffer();
		
		for (String key : vertexLabelToIndexMap.keySet()) {
			vertices.append(key + " ");	
		}
		
		os.println(vertices.toString().trim());
		
		os.flush();
		
	} // end of printVertices()

	public void printEdges(PrintWriter os) {
		int mapValue;
		
		if (vertexLabelToIndexMap.isEmpty()) return;
		
		for (Map.Entry<String, Integer> item : vertexLabelToIndexMap.entrySet()) {
			mapValue = item.getValue();
			if(myAdjList[mapValue].mLength > 0){
				os.println(myAdjList[mapValue].print(item.getKey()));
			}
		}
		
		os.flush();

	} // end of printEdges()

	public int shortestPathDistance(T vertLabel1, T vertLabel2) {	
		if(!vertexExist(vertLabel1) || !vertexExist(vertLabel2)) return disconnectedDist;
		
		String SourceVertex = (String)vertLabel1;
		String DestinationVertex = (String)vertLabel2;
		Queue<VertexTracker> queue = new LinkedList<VertexTracker>();
		Map<String, VertexTracker> verticesTracker = new HashMap<String, VertexTracker>();
		ArrayList<T> neighbours;
		
		// Load all vertices to keep track
		verticesTracker = LoadAllVertices(SourceVertex);
		
		// add source vertex to queue
		queue.add(new VertexTracker(SourceVertex,true, 0));
		
		while(queue.size() > 0) {
			
			VertexTracker poppedVertex = queue.poll();
			
			//get all adjacent vertices of current vertex
			neighbours = neighbours((T)poppedVertex.getVertexValue());
			
			// check if SourceVertex or DestinationVertex are disconnected from the adjacency list, return early
			if(neighbours.size() == 0 && 
			  (poppedVertex.getVertexValue().equals(SourceVertex)) || poppedVertex.getVertexValue().equals(DestinationVertex)) return disconnectedDist;
			
			for(T neighbour : neighbours) {
				VertexTracker vertex = verticesTracker.get(neighbour.toString().replace(",", ""));
				
				// found shortest path!
				if(vertex.getVertexValue().equals(DestinationVertex)) return poppedVertex.GetDistance() + 1;
				
				// haven't found shortest path yet, continue to traverse through vertices
				if(!vertex.IsVisited()) {
					vertex.SetIsVisited(true);
					vertex.SetDistance(poppedVertex.GetDistance() + 1);
					queue.add(new VertexTracker(vertex));
				}	
			}
		}
		// if we reach this point, source and target are disconnected
		return disconnectedDist;
	} // end of shortestPathDistance()
	
	private Map<String, VertexTracker> LoadAllVertices(String sourceVertex) {
		Map<String, VertexTracker> adjList = new HashMap<String, VertexTracker>();
		
		for (Map.Entry<String, Integer> vertex : vertexLabelToIndexMap.entrySet()) {
			boolean visited = false;
			if(vertex.getKey().equals(sourceVertex)){
				visited = true;
			}
			
			adjList.put(vertex.getKey(), new VertexTracker(vertex.getKey(),visited, 0));
			
			visited = false;
		}
		
		return adjList;
	}

	private void removeEdgeFromVertex(T srcLabel, T tarLabel) {
		
		if(canEdgeBeAdded(srcLabel,tarLabel)) return;
		
		myAdjList[vertexLabelToIndexMap.get(srcLabel)].remove((String) tarLabel);
		myAdjList[vertexLabelToIndexMap.get(tarLabel)].remove((String) srcLabel);

	}
	
	private void addNewVertexToAdjList(T vertLabel) {
		myAdjListExpand();
		
		vertexLabelToIndexMap.put((String)vertLabel, verticesCount);
		myAdjList[verticesCount] = new MyLinkedList();
	  
		verticesCount++;
	}
	
	private void removeVertexFromAdjList(T vertLabel) {
		
		if(!vertexExist(vertLabel)) return;
		
		int verticeIndexToRemove = vertexLabelToIndexMap.get(vertLabel);
		
		vertexLabelToIndexMap.remove(vertLabel);
		
		verticesCount--;
		
		removeEdgeFromOtherVertices(vertLabel);

		refreshAdjListAfterVerticeRemoval(verticeIndexToRemove);
	}
	
	private void removeEdgeFromOtherVertices(T vertLabel) {
		for (Map.Entry<String, Integer> item : vertexLabelToIndexMap.entrySet()) {
				myAdjList[item.getValue()].remove((String)vertLabel);
		}	
	}

	private void myAdjListExpand() {
		
		if(arrayCapacityCurrent >= verticesCount + 1){
		    return;
		}
		
		arrayCapacityCurrent = ((arrayCapacityCurrent * 3) / 2)  + 1;
		
		MyLinkedList[] tempMyAdjList = new MyLinkedList[arrayCapacityCurrent];
		
		int newArrayIndex = 0;
		
		for (int i = 0; i < myAdjList.length; i++) {
				tempMyAdjList[newArrayIndex] = myAdjList[i];
				newArrayIndex ++;
		}

		myAdjList = tempMyAdjList;
	}
	
	private void refreshAdjListAfterVerticeRemoval(int verticeIndexToRemove) {
	
		if(verticesCount == 0){
			myAdjList[0] = null;
		}
		
		//align vertexLabelToIndexMap to AdjList
		
		for (int i = verticeIndexToRemove; i <  verticesCount; i++) {
			myAdjList[i] = myAdjList[i + 1];
		}
		
		for (Map.Entry<String, Integer> item : vertexLabelToIndexMap.entrySet()) {
			if(item.getValue() > verticeIndexToRemove){
				item.setValue(item.getValue() - 1);
			} 
		}
	}
	
	private boolean vertexExist(T vertLabel) {

		if(vertexLabelToIndexMap.isEmpty()) return false;
		
		if(vertexLabelToIndexMap.containsKey(vertLabel)) return true;
		
		return false;
	}
	
	private boolean canEdgeBeAdded(T srcLabel, T tarLabel) {
		
		if(srcLabel.equals(tarLabel)) {
			systemError("Source Vertex '" +  srcLabel + "' cannot have an edge to itself.");
			return false;
		}
		
		boolean sourceVertexExists = vertexExist(srcLabel);

		if (!sourceVertexExists) {
			systemError("Source Vertex '" +  srcLabel + "' Does Not Exist");
			return false;
		}

		boolean targetVertexExists = vertexExist(tarLabel);
		
		if (!targetVertexExists) {
			systemError("Target Vertex '" +  tarLabel + "' Does Not Exist");
			return false;
		}
		
		if (edgeAlreadyExistsForSource(srcLabel, tarLabel)) {
			return false;
		}
		
		return true;
	}
	
	private boolean edgeAlreadyExistsForSource(T srcLabel, T tarLabel) {
	   return (myAdjList[vertexLabelToIndexMap.get(srcLabel)].search((String)tarLabel) ? true : false);
	}

	private void addNewEdgeToVertex(T vertLabel, T edge) {
		myAdjList[vertexLabelToIndexMap.get((String)vertLabel)].add((String)edge);
		myAdjList[vertexLabelToIndexMap.get((String)edge)].add((String)vertLabel);
	}
	
	private ArrayList<T> getAllNeighbours(T vertLabel){
		ArrayList<T> neighbours = new ArrayList<T>();
		String separator = "";

		if(!vertexExist(vertLabel)) throw new IllegalArgumentException();
			
		ArrayList<T> neighboursOnVertex = getNeighboursOnVertex(vertLabel, separator);
			
		neighbours.addAll(neighboursOnVertex);	
		
		return neighbours;
	}
	
	private ArrayList<T> getNeighboursOnVertex(T vertLabel, String separator){
		ArrayList<T> neighboursOnVertex = new ArrayList<T>();
		Boolean firstSet = false;
		
		MyNode currNode = myAdjList[vertexLabelToIndexMap.get(vertLabel)].getFirst();
		while(currNode != null) {
			if(firstSet == true) separator = ",";
			neighboursOnVertex.add((T)(separator + currNode.getValue()).trim());
			currNode = currNode.getNext();
			firstSet = true;
		}
		
		return neighboursOnVertex;
	}
	
	private void systemError(String string) {
		System.err.println(string);
	}

} // end of class AdjList