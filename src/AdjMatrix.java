import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Adjacency matrix implementation for the FriendshipGraph interface.
 * <p>
 * Your task is to complete the implementation of this class.  You may add methods, but ensure your modified class compiles and runs.
 *
 * @author Jeffrey Chan, 2016.
 */

@SuppressWarnings("unchecked")
public class AdjMatrix<T extends Object> implements FriendshipGraph<T> {

    private boolean[][] adjMatrix;   // adjacency matrix
    private CustomArray<T> vertices;   // values of vertices
    CustomArray<T> shortestPath = new CustomArray<T>();

    /**
     * Contructs empty graph.
     */
    public AdjMatrix() {
        // Implement me!
        this.adjMatrix = new boolean[5][5];
        this.vertices = new CustomArray<T>();
    } // end of AdjMatrix()

    public void addVertex(T vertLabel) {
        // Implement me!
        if (!vertexExists(vertLabel)) {

            vertices.add(vertLabel);

            checkAdjMatrixCapacity();

            for (int i = 0; i < vertices.size(); i++) {
                adjMatrix[vertices.size() - 1][i] = false;
                adjMatrix[i][vertices.size() - 1] = false;
            }
        }

    } // end of addVertex()

    public void addEdge(T srcLabel, T tarLabel) {
        // Implement me!
        if (vertexExists(srcLabel) && vertexExists(tarLabel)) {
            adjMatrix[vertices.getIndexOf(srcLabel)][vertices.getIndexOf(tarLabel)] = true;
            adjMatrix[vertices.getIndexOf(tarLabel)][vertices.getIndexOf(srcLabel)] = true;
        }

    } // end of addEdge()


    public ArrayList<T> neighbours(T vertLabel) {
        ArrayList<T> neighbours = new ArrayList<T>();

        // Implement me!
        if (!vertexExists(vertLabel)) throw new IllegalArgumentException();

        int labelIndex = vertices.getIndexOf(vertLabel);

        for (int i = 0; i < vertices.size(); i++) {
            if (adjMatrix[labelIndex][i]) {
                neighbours.add(vertices.get(i));
            }
        }

        return neighbours;
    } // end of neighbours()


    public void removeVertex(T vertLabel) {
        // Implement me!
        if (vertexExists(vertLabel)) {
            int removedVertexIndex = vertices.getIndexOf(vertLabel);

            reallocateEdges(removedVertexIndex);
        }

    } // end of removeVertex()

    public void removeEdge(T srcLabel, T tarLabel) {
        // Implement me!
        if (vertexExists(srcLabel) && vertexExists(tarLabel)) {
            adjMatrix[vertices.getIndexOf(srcLabel)][vertices.getIndexOf(tarLabel)] = false;
            adjMatrix[vertices.getIndexOf(tarLabel)][vertices.getIndexOf(srcLabel)] = false;
        }
    } // end of removeEdges()


    public void printVertices(PrintWriter os) {
        // Implement me!
        if (vertices != null) {
            for (int i = 0; i < vertices.size(); i++) {
                CharSequence cs = (CharSequence) vertices.get(i) + " ";
                os.append(cs);
                os.flush();
            }
        }
    } // end of printVertices()

    public void printEdges(PrintWriter os) {
        // Implement me!
        if (adjMatrix != null) {
            for (int i = 0; i < vertices.size(); i++) {
                for (int c = 0; c < vertices.size(); c++) {
                    if (adjMatrix[i][c]) {
                        CharSequence cs = (CharSequence) vertices.get(i) + " " + vertices.get(c) + "\n";
                        os.append(cs);
                        os.flush();
                    }
                }
            }
        }
    } // end of printEdges()


    public int shortestPathDistance(T vertLabel1, T vertLabel2) {
        if (!vertexExists(vertLabel1) || !vertexExists(vertLabel2)) return disconnectedDist;

        String SourceVertex = (String) vertLabel1;
        String DestinationVertex = (String) vertLabel2;
        Queue<VertexTracker> queue = new LinkedList<VertexTracker>();
        Map<String, VertexTracker> verticesTracker = new HashMap<String, VertexTracker>();
        ArrayList<T> neighbours;

        // Load all vertices to keep track
        verticesTracker = LoadAllVertices(SourceVertex);

        // add source vertex to queue
        queue.add(new VertexTracker(SourceVertex, true, 0));

        while (queue.size() > 0) {

            VertexTracker poppedVertex = queue.poll();

            //get all adjacent vertices of current vertex
            neighbours = neighbours((T) poppedVertex.getVertexValue());

            // check if SourceVertex or DestinationVertex are disconnected from the adjacency list, return early
            if (neighbours.size() == 0 &&
                    (poppedVertex.getVertexValue().equals(SourceVertex)) || poppedVertex.getVertexValue().equals(DestinationVertex))
                return disconnectedDist;

            for (T neighbour : neighbours) {
                VertexTracker vertex = verticesTracker.get(neighbour.toString().replace(",", ""));

                // found shortest path!
                if (vertex.getVertexValue().equals(DestinationVertex)) return poppedVertex.GetDistance() + 1;

                // haven't found shortest path yet, continue to traverse through vertices
                if (!vertex.IsVisited()) {
                    vertex.SetIsVisited(true);
                    vertex.SetDistance(poppedVertex.GetDistance() + 1);
                    queue.add(new VertexTracker(vertex));
                }
            }
        }
        // if we reach this point, source and target are disconnected
        return disconnectedDist;
    } // end of shortestPathDistance()

    private Map<String, VertexTracker> LoadAllVertices(String sourceVertex) {
        Map<String, VertexTracker> matrix = new HashMap<String, VertexTracker>();

        for (int i = 0; i < vertices.size(); i++) {
            boolean visited = false;
            if (vertices.get(i).equals(sourceVertex)) {
                visited = true;
            }

            matrix.put(vertices.get(i).toString(), new VertexTracker(vertices.get(i).toString(), visited, 0));

            visited = false;
        }

        return matrix;

    }


    //check if vertex exists in array
    public boolean vertexExists(T label) {

        for (int i = 0; i < vertices.size(); i++) {
            if (vertices.get(i).equals(label)) {
                return true;
            }
        }
        return false;
    }

    //check if matrix has less than 5 slots left
    public void checkAdjMatrixCapacity() {
        if (adjMatrix.length - vertices.size() < 5)
            expandAdjMatrix();
    }

    //expand adjMatrix 2D array
    public void expandAdjMatrix() {
        boolean tempMat[][] = new boolean[((adjMatrix.length * 3) / 2) + 1][((adjMatrix.length * 3) / 2) + 1];

        for (int i = 0; i < adjMatrix.length; i++) {
            for (int c = 0; c < adjMatrix.length; c++) {
                tempMat[i][c] = adjMatrix[i][c];
            }
        }

        adjMatrix = tempMat;
    }


    public void reallocateEdges(int labelIndex) {

        int verticesSize = vertices.size();
        // Swap the vertex that we want to remove with the last vertex in list
        vertices.swapElements(labelIndex, verticesSize - 1);

        // Removing the vertex which is now at the end of the list
        vertices.remove(verticesSize - 1);

        for (int j = 0; j < labelIndex; j++) {
            // Reallocating the old removed-label index elements of the last row in the adjMatrix into its new index
            adjMatrix[labelIndex][j] = adjMatrix[verticesSize - 1][j];
            adjMatrix[j][labelIndex] = adjMatrix[j][verticesSize - 1];
        }

        for (int j = labelIndex + 1; j < verticesSize - 1; j++) {
            // Move the remaining elements of the last row in the adjMatrix into the removed-label index collumn
            adjMatrix[j][labelIndex] = adjMatrix[verticesSize - 1][j];
            adjMatrix[labelIndex][j] = adjMatrix[j][verticesSize - 1];
        }

        // Set edges in last row in the matrix to false
        for (int k = 0; k < verticesSize; k++) {
            adjMatrix[verticesSize - 1][k] = false;
            adjMatrix[k][verticesSize - 1] = false;
        }
    }

} // end of class AdjMatrix