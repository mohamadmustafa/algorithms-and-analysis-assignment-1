import java.util.Arrays;

public class CustomArray<E> {

    private static final int DEFAULT_INITIAL_CAPACITY = 5;

    private static final Object[] EMPTY_ELEMENT_DATA = {};

    private int size;

    /**
     * The array elements to be stored inside
     * customArrayListElementData.
     */
    private transient Object[] customArrayElement;

    /**
     * Constructs an empty list.
     */
    public CustomArray() {
        super();
        this.customArrayElement = EMPTY_ELEMENT_DATA;
    }

    /**
     * @return the size of the CustomArrayList
     */
    public int size() {
        return size;
    }

    /**
     * return true
     * @param e
     */
    public boolean add(E e) {

        ensureCapacity(size + 1);
        customArrayElement[size++] = e;

        return true;
    }

    /**
     * Returns the index of the element in this list.
     *
     * @param element
     * @return
     */
    public int getIndexOf(E element) {

        int noElement = -1;

        for (int i = 0; i < customArrayElement.length; i++) {
            if (customArrayElement[i].equals(element)) {
                return i;
            }
        }
        return noElement;
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index
     * @return
     */
    @SuppressWarnings("unchecked")

    public E get(int index) {

        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("array index out of bound exception with index at" + index);
        }

        return (E) customArrayElement[index];
    }

    /**
     * add element at specific index position and shift the
     * customArrayListElementData.
     *
     * @param index
     * @param element
     */
    public void add(int index, E element) {

        ensureCapacity(size + 1);
        System.arraycopy(customArrayElement, index, customArrayElement, index + 1, size - index);
        customArrayElement[index] = element;
        size++;
    }

    /**
     * Remove the element from the customArrayListElementData
     * and shift the elements position.
     *
     * @param index
     * @return
     */
    @SuppressWarnings("unchecked")

    public E remove(int index) {

        E oldValue = (E) customArrayElement[index];
        int removeNumber = size - index - 1;
        if (removeNumber > 0) {
            System.arraycopy(customArrayElement, index + 1, customArrayElement, index, removeNumber);
        }

        customArrayElement[--size] = null;

        return oldValue;
    }

    /**
     * Increases the capacity to ensure that it can hold at least the
     * number of elements specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     */
    private void growCustomArrayList(int minCapacity) {

        int oldCapacity = customArrayElement.length;
        int newCapacity = oldCapacity + (oldCapacity / 2);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;

        customArrayElement = Arrays.copyOf(customArrayElement, newCapacity);
    }

    /**
     * ensure the capacity and grow the customArrayList vi
     * growCustomArrayList(minCapacity);
     *
     * @param minCapacity
     */
    private void ensureCapacity(int minCapacity) {

        if (customArrayElement == EMPTY_ELEMENT_DATA) {
            minCapacity = Math.max(DEFAULT_INITIAL_CAPACITY, minCapacity);
        }

        if (minCapacity - customArrayElement.length > 0)
            growCustomArrayList(minCapacity);
    }

    /**
     * swaps two elements in the array using their indices
     *
     * @param src
     * @param dist
     */
    public void swapElements(int src, int dist){
        Object tempValue = customArrayElement[src];

        customArrayElement[src] = customArrayElement[dist];
        customArrayElement[dist] = tempValue;
    }
}

