
public interface ListInteractors {
 
	public abstract void add(String element);
	
	public abstract MyNode getFirst();
	
	public abstract boolean search(String value);
	
	public abstract boolean remove(String value);
	
}
