public class MyLinkedList implements ListInteractors {

	/** Reference to head of list. */
    protected MyNode mHead;
    /** Reference to tail of list. */
    protected MyNode mTail;

    /** Length of list. */
    protected int mLength;

    public MyLinkedList() {
        mHead = null; 
        mTail = null;
        mLength = 0;
    } // end of DoubleLinkedList()
    
	
	@Override
	public void add(String element) {
		 MyNode newNode = new MyNode(element);

         if (mHead == null) {
             mHead = newNode;
             mTail = newNode;
         }
         else {
        	 // always insert new node as the Head
            	MyNode previousHead = mHead;
            	mHead = newNode;
            	newNode.setNext(previousHead);
            	previousHead.setPrev(mHead);      
             }

         mLength++;
	}


	@Override
	public MyNode getFirst() {
	  return mHead;
	}

	@Override
	public boolean search(String value) {
        return (getNode(value) != null ? true : false);
	}
	
	public StringBuffer print(String vertice) { 
		MyNode currNode = mHead;
		String newLine = System.getProperty("line.separator");
		StringBuffer str = new StringBuffer();

        while (currNode != null) {
        	if(currNode.getNext() == null) newLine = "";
        	str.append(vertice + " " + currNode.getValue() + newLine);
            currNode = currNode.getNext();
        }   
        
        return str;
	}

	@Override
	public boolean remove(String value) {
		
		MyNode nodeToRemove = getNode(value);
		
		if(nodeToRemove == null) return false;

		if(mLength == 1){
			mHead = null;
			mTail = null;
		} 
		else if (nodeToRemove == mTail){
			mTail = nodeToRemove.getPrev();
			mTail.setNext(null);
		} else if (nodeToRemove == mHead) {
			mHead = nodeToRemove.getNext();
			mHead.setPrev(null);
		} else{
			MyNode nodeNext = nodeToRemove.getNext();
			MyNode nodePrevious = nodeToRemove.getPrev();

			nodeNext.setPrev(nodePrevious);
			nodePrevious.setNext(nodeNext);
		}
		
		mLength--;
		
		return true;
	}
	
	private MyNode getNode(String value) {
		
		MyNode currNode = mHead;
		
		MyNode foundNode = null;
		
		 while (currNode != null) {
	        	if(currNode.getValue().equals(value)) {
	        		foundNode = currNode;
	        		break;
	        	}
	        	currNode = currNode.getNext();
	        }   
		 
		 return foundNode;
	}

}
