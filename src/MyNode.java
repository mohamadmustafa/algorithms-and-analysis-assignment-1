
    public class MyNode
    {
        private String mValue;
        
        private MyNode mNext;
       
        private MyNode mPrev;

        public MyNode(String value) {
            mValue = value;
            mNext = null;
            mPrev = null;
        }

        public String getValue() {
            return mValue;
        }

        public MyNode getNext() {
            return mNext;
        }
        
        public MyNode getPrev() {
            return mPrev;
        }

        public void setValue(String value) {
            mValue = value;
        }

        public void setNext(MyNode next) {
            mNext = next;
        }
        
        public void setPrev(MyNode prev) {
            mPrev = prev;
        }
    } 
	