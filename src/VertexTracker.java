
public class VertexTracker {
    private String vertexValue;
    private boolean isVisited;
    private int distance;
   
    public VertexTracker(String vertexValue, boolean isVisited, int distance) {
    	this.vertexValue = vertexValue;
    	this.distance = distance;
    	this.isVisited = isVisited;
    }

	public VertexTracker(VertexTracker vertex) {
		this.vertexValue = vertex.getVertexValue();
    	this.distance = vertex.GetDistance();
    	this.isVisited = vertex.IsVisited();
	}

	public String getVertexValue() {
		return vertexValue;
	}
	
	public boolean IsVisited() {
		return isVisited;
	}

	public int GetDistance() {
		return distance;
	}
	
	public void SetIsVisited(boolean value) {
		this.isVisited = value;
	}
	
	public void SetDistance(int value) {
		this.distance = value;
	}

}
